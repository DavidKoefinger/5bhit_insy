﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simplex
{
    class Program
    {
        static SimplexContext.SimplexDataContext sc = new SimplexContext.SimplexDataContext();



        static void Main(string[] args)
        {
            var endpro = from x in sc.Produkts select x;

            var ausgpro = from z in sc.Zutats select z;

            var zwischentabelle = from y in sc.ProduktHasZutats select y;

            Simplex simplex = new Simplex(endpro.Count(), ausgpro.Count());

            int[] produkte = new int[endpro.Count()];
            int[] zutaten= new int[ausgpro.Count()];
            double[,] produktzutaten = new double[endpro.Count(),ausgpro.Count()];

            int i = 0;
            int j = 0;

            foreach (var item in endpro)
            {
                produkte[i] = Int32.Parse(item.Produktmenge.ToString());
                i++;
            }
            i = 0;
            foreach (var item in ausgpro)
            {
                zutaten[i] = Int32.Parse(item.Menge.ToString());
                i++;
            }
            i = 0;

            foreach (var item in zwischentabelle)
            {
                if (j !=0 && (j % produkte.Count()) == 0)
                {
                    i++;
                    j = 0;
                }
                produktzutaten[i, j] = Double.Parse(item.Zutatmenge.ToString());
                j++;
            }
            

            // mit lauter "0" ausgeben
           
           
            simplex.fillLine(0, new double[] { produkte[0], produkte[1], 0 });
            simplex.fillLine(1, new double[] { produktzutaten[0,0], produktzutaten[1,0], zutaten[0] });
            simplex.fillLine(2, new double[] { produktzutaten[0, 1], produktzutaten[1, 1], zutaten[1] });

            simplex.quotientenBerechnen();
            Console.WriteLine(simplex.ToString());
            Console.WriteLine("--------------------------------------------------------------------------");

            // solange durchlaufen, bis fertig!
            simplexDurchfuehren(simplex);

            //simplex.dividierePivot();
            //simplex.quotientenBerechnen();
            //Console.WriteLine(simplex.ToString());
            //Console.WriteLine("--------------------------------------------------------------------------");
            //simplex.dividierePivot();
            //simplex.quotientenBerechnen();
            //Console.WriteLine(simplex.ToString());
            //Console.WriteLine("--------------------------------------------------------------------------");
            //simplex.dividierePivot();
            //simplex.quotientenBerechnen();
            //Console.WriteLine(simplex.ToString());


            Console.ReadKey();
        }

        static void simplexDurchfuehren(Simplex s)
        {
            while(s.getState() == false)
            {
                s.dividierePivot();
                s.quotientenBerechnen();
                Console.WriteLine(s.ToString());
                Console.WriteLine("--------------------------------------------------------------------------");
            }
        }
    }
}
